# Kubernetes - Technical Architecture Solution Delivery: Cluster Storage
# Hands-on Exercises #06

### Objective



### Parts

[Part 1: ConfigMaps](Part-01-ConfigMaps.md)

[Part 2: Secrets](Part-02-Secrets.md)

[Part 3: Volumes](Part-03-Volumes.md)

[Part 4: StatefulSets](Part-04-StatefulSets.md)

Return to the course [Table of Content](../README.md)
