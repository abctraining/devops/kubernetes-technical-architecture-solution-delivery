# Kubernetes - Technical Architecture Solution Delivery: Monitoring
# Hands-on Exercises #09

### Objective



### Parts

[Part 1: EFK](Part-01-EFK.md)

[Part 2: Prometheus](Part-02-Prometheus.md)

Return to the course [Table of Content](../README.md)
