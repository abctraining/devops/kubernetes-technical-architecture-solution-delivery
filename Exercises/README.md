# Kubernetes - Technical Architecture Solution Delivery - Labs

* [00 - Connect to the Labs](00-LabConnection) - Lab connection
* [01 - Alternative Containers](01-AltContainers) - Buildah, Podman, Nertctl
* [02 - K8s Distributions](02-K8sDistributions) - MicroK8s - RKE - Kind
* [03 - LoadBalancer and Ingress](03-LoadBalancerAndIngress) - MetalLB - Ingress - IngressClasses
* [04 - Release Strategy](04-ReleaseStrategy) - Istio - Virtual Services - Canary
* [05 - Deployment Control](05-DeploymentControl) - Jobs / Cronjobs - Probes - HPA
* [06 - Cluster Storage](06-ClusterStorage) -  ConfigMaps - StorageClasses - Volumes
* [07 - Backups](07-Backups) - Velero
* [08 - Workflow Automantion](08-Automation) - Terraform - Ansible
* [09 - Monitoring](09-Monitoring) - EFK - Prometheus - Grafana

---

###### Lab Files

_Copy Lab repo local_
```bash
mkdir -p ~/content
cd ~/content
git clone https://gitlab.com/abctraining/devops/kubernetes-technical-architecture-solution-delivery.git
cd kubernetes-technical-architecture-solution-delivery
```
