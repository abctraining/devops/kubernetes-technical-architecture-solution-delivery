# Exercise #04 : Part 2
## Virtual Services

---

### Intro

"Configuration affecting traffic routing. Here are a few terms useful to define in the context of traffic routing."

###### Gateway

Configures a loadBalancer for HTTP/TCP trafic, most commonly operating at the edge of the mesh to inable ingress traffic for an application.

###### VirtualService

Controls how requests for a service are routed within an Istio service mesh.

###### DestinationRule

Configures the set of policies to be applied to the request after VirtualService routing has occured.

###### ServiceEntry

Commonly used to enabel requests to services outside of an Istio service mesh.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/04-ReleaseStrategy
~~~

### Virtual Services



We will use the Bookshelf app to demo the virtual services in Istio.  We first need a namespace.

```bash
kubectl create namespace bookinfo
kubectl label namespace bookinfo istio-injection=enabled
```

The "envoy" proxy should now be set to be injected.

```bash
kubectl get namespace -L istio-injection
```

```bash
kubectl -n bookinfo apply -f bookinfo.yaml
```

We can watch the app deploy.

```bash
watch kubectl -n bookinfo get all
```

Here is the first VirtualService defined in the 'bookinfo-gateway.yaml' file.  Notice how the VirtualService matches the routing for traffic comming in.

_bookinfo-gateway.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: bookinfo-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
  - "*"
  gateways:
  - bookinfo-gateway
  http:
  - match:
    - uri:
        exact: /productpage
    - uri:
        prefix: /static
    - uri:
        exact: /login
    - uri:
        exact: /logout
    - uri:
        prefix: /api/v1/products
    route:
    - destination:
        host: productpage
        port:
          number: 9080
```

Let us apply that 'bookinfo-gateway.yaml' manifest

```bash
kubectl -n bookinfo apply -f bookinfo-gateway.yaml
```

We can list the gateway object

```bash
kubectl -n bookinfo get gateway
```

and the VirtualService

```bash
kubectl -n bookinfo get virtualservices
```

#### Connect to http://(PublicIP)/productpage

Refresh and notice the ratings

---

Now we can explore some more options.

_virtual-service-all-v1.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage
spec:
  hosts:
  - productpage
  http:
  - route:
    - destination:
        host: productpage
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: details
spec:
  hosts:
  - details
  http:
  - route:
    - destination:
        host: details
        subset: v1
---
```

```bash
kubectl -n bookinfo apply -f virtual-service-all-v1.yaml
```

```bash
kubectl -n bookinfo get virtualservices -o yaml | less
```

_destination-rule-all.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage
spec:
  host: productpage
  subsets:
  - name: v1
    labels:
      version: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews
spec:
  host: reviews
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v3
    labels:
      version: v3
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: ratings
spec:
  host: ratings
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v2-mysql
    labels:
      version: v2-mysql
  - name: v2-mysql-vm
    labels:
      version: v2-mysql-vm
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: details
spec:
  host: details
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
```

```bash
kubectl -n bookinfo apply -f destination-rule-all.yaml
```

```bash
kubectl -n bookinfo get destinationrules -o yaml | less
```

#### Connect to http://{PublicIP}/productpage
Refresh and notice the ratings, all sent to "v1"

---

_virtual-service-reviews-test-v2.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - match:
    - headers:
        end-user:
          exact: jason
    route:
    - destination:
        host: reviews
        subset: v2
  - route:
    - destination:
        host: reviews
        subset: v1
```

```bash
kubectl -n bookinfo apply -f virtual-service-reviews-test-v2.yaml
```

```bash
kubectl -n bookinfo get virtualservice reviews -o yaml | less
```

#### Connect to http://{PublicIP}/productpage

- signin with jason:password

- signout and backin with jack:password

Notice the difference.  Now look back on the VirtualService above.

---

_virtual-service-ratings-test-delay.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - match:
    - headers:
        end-user:
          exact: jason
    fault:
      delay:
        percentage:
          value: 100.0
        fixedDelay: 7s
    route:
    - destination:
        host: ratings
        subset: v1
  - route:
    - destination:
        host: ratings
        subset: v1
```

```bash
kubectl -n bookinfo apply -f virtual-service-ratings-test-delay.yaml
```

```bash
kubectl -n bookinfo get virtualservice ratings -o yaml
```

#### Connect to http://{PublicIP}/productpage

- sign in as jason:password

- notice the delay / timeout / error

Again look back at the above "VirtualService"

Let us remove the VirtualService with a delay.

```bash
kubectl -n bookinfo delete -f virtual-service-ratings-test-delay.yaml
```

And Set it back to "v1"

```bash
kubectl -n bookinfo apply -f virtual-service-all-v1.yaml
```

- notice everything is routed to "v1"

---

This concludes part 2 of Hands-on Exercise #04.  Continue on to Part 3 next.

[Part 3: Canary](Part-03-Canary.md)
