# Exercise #04 : Part 3
## Canary

---

### Intro

Canary releases allow testing of functionality to a small percentage of users before releasing to all users.

---

### Canary

We will pickup where we have left off and use a "VirtualService" to allow for a "Canary" release.

We will start by sending 25% of the traffic to the new version of the release.

_virtual-service-reviews-25-v3.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
      weight: 75
    - destination:
        host: reviews
        subset: v3
      weight: 25
```

```bash
kubectl -n bookinfo apply -f virtual-service-reviews-25-v3.yaml
```

#### Connect to http://{PublicIP}/productpage

- notice %75 - %25 of requests go between "v1" and "v3" (Red)

---

Let us send a bit more traffic to "v3".

_virtual-service-reviews-50-v3.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
      weight: 50
    - destination:
        host: reviews
        subset: v3
      weight: 50
```

```bash
kubectl -n bookinfo apply -f virtual-service-reviews-50-v3.yaml
```

#### Connect to http://{PublicIP}/productpage

- notice %50 of requests go between "v1" and "v3" (Red)

---

This will send all traffic to "v3" once we know that "v3" is working well because of the "Canary"

_virtual-service-reviews-v3.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v3
```

And finalize the chnage to "v3"

```bash
kubectl -n bookinfo apply -f virtual-service-reviews-v3.yaml
```

---

Clean up everything by revoing the "bookinfo" namespace.

```bash
kubectl delete namespace bookinfo
```

We will also be switching back to the RKE2 cluster.

```bash
cp $HOME/.kube/config-rke2 $HOME/.kube/config
```

---

This concludes part 3 of Hands-on Exercise #04.

Return to the [Exercise Page](../README.md)
