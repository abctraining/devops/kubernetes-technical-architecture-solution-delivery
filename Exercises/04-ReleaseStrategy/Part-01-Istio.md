# Exercise #04 : Part 1
## Istio

---

### Intro

"Simplify observability, traffic management, security, and policy with the leading service mesh."

---

To test Istio "VirtualServices" we really need to render a webpage in a browser.  The simplest way to do that would be to take advantage of the Public IP of the primary lab instance combined with the "metallb" configured as the MicroK8s LoadBalancer.  You should still have MicroK8s running on your primary Lab instance, great let's us it.  If not please [install it from the labs](../02-K8sDistributions/Part-02-MicroK8s.md).  You may need to remove an existing Kubernetes cluster first.  The end goal is to have MicroK8s installed on the primary lab instance with MetalLB setup to use the private IP of your primary lab instance.

Once you know you have MicroK8s installed on your primary Lab instance your kubectl config will have been changed to point to the single node cluster.


```bash
cp $HOME/.kube/config-microk8s $HOME/.kube/config
```

You can verify that with 'kubectl get nodes'.

```bash
kubectl get nodes
```

Let us store this config in a separate file to make it simpler to go back and forth between MicroK8s clusters.  There is a "kubectl config" option that allows different "context" to be stored in a single config file, but we will not be covering that here.  For now we will just swap configs as needed.

__Verify that your "kubectl" is currently configured for your single node MicroK8s cluster__

The following command should return a single node, your primary node.

```bash
kubectl get nodes
```

---

##### Install Istio

Let us enable the Istio plugin that adds Istio to MicroK8s.


```bash
microk8s enable community
microk8s enable istio
```

If everything went well we should be able to verify that the "EXTERNAL-IP" of the 'service/istio-ingressgateway' is set to the "eth0" of your primary lab instance.

```bash
kubectl -n istio-system get service istio-ingressgateway
```

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/04-ReleaseStrategy
~~~

##### Import MicroK8s cluster into Rancher

We have a rancher instance available let us add the MicroK8s cluster to it. From the Rancher UI:

1.	From the "Home" Dashboard, click "Import Existing" in the Cluster section.

1.	Choose "Generic", to import any K8s Cluster.

1.	Enter a Cluster Name including your name to easily identify it as your microk8s cluster.

1.	Click Create.

1.	Copy the kubectl command to your clipboard and run it on a node where kubeconfig is configured to point to the cluster you want to import. If you are unsure it is configured correctly, run kubectl get nodes to verify before running the command shown in Rancher.

1.	If you are using self signed certificates, you will receive the message certificate signed by unknown authority. To work around this validation, copy the command starting with curl displayed in Rancher to your clipboard. Then run the command on a node where kubeconfig is configured to point to the cluster you want to import.

1.	When you finish you will see your cluster listed as "Active"

1.  To access the cluster dashboard select the three horizontal lines on the top left of the page, then select your cluster.

---

## Test out Istio

Istio uses the a label "istio-injection" to determine what pods will have the "envoy" proxy injected.  We can view that label value by adding a colomn for it with 'kubectl get namespaces'.

```bash
kubectl get namespace -L istio-injection
```

Create a few namespaces to work with.

```bash
kubectl create namespace servicemesh
kubectl create namespace no-servicemesh
```

Add the appropriate label to the "servicemesh" namespace.

```bash
kubectl label namespace servicemesh istio-injection=enabled
```

Now notice the label when listing "namespaces"

```bash
kubectl get namespace -L istio-injection
```

Let us install a simple sleep service into the "no-servicemesh" namespace.

```bash
kubectl -n no-servicemesh apply -f sleep.yaml
```

Now observe the resulting pod.  Notice that in the "READY" colomn there is "1/1".  That is 1 container is READY of the expected 1 containers in that pod.  By this we know that no "sidecart" container was injected.

```bash
kubectl -n no-servicemesh get pods
```

We could also "describe" the container to get the same information.

```bash
kubectl -n no-servicemesh describe pod sleep-<POD-ID>
```

Now apply the same sleep.yaml manifest this time to the "servicemesh" namespace.

```bash
kubectl -n servicemesh apply -f sleep.yaml
```

And list the pods, the "READY" column should be 2/2.

```bash
kubectl -n servicemesh get pods
```

notice the "sidecart" in the description of the pod.

```bash
kubectl -n servicemesh describe pod sleep-<POD-ID>
```

To remove the label from a namespace append a "-" to the end of it.

```bash
kubectl label namespace servicemesh istio-injection-
```

Verify that the label has been removed

```bash
kubectl get namespace -L istio-injection
```

You will notice that there still is a "sidecart" container in the pods.  This is because the "injection" happens only when a pod is scheduled.  Existing pods stay as is.  The "sidecart" is not part of the "PodSpec".

```bash
kubectl -n servicemesh get pods
```

We can simply remove the namespaces to remove the resounces in those namespaces and cleanup.

```bash
kubectl delete namespace no-servicemesh
kubectl delete namespace servicemesh
```

---

This concludes part 1 of Hands-on Exercise #04.  Continue on to Part 2 next.

[Part 2: VirtualServices](Part-02-VirtualServices.md)
