# Exercise #01 : Part 3
## podman

---

### Intro

We will explore managing containers with podman in this exercise.

---

### podman

Let us first install podman.

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y podman
```

Show some basic information about the podman install.

```bash
podman info
```

And the online help.  Look through these options.  Notice they are very similar to the Docker CLI options.

```bash
podman --help
```

We can list the images in the local image-stor.

```bash
podman image ls
```

We can inspect that image that we created with buildah earlier.  Notice the image name includes "localhost" to clearly indicate where it is located.

```bash
podman image inspect localhost/fedora-nginx
```

We can also inspect the fedora image that was pulled from upstream.

```bash
podman image inspect registry.fedoraproject.org/fedora
```

Similar to the other tools we can "format" the inspect output to return specific "JSON" items.

```bash
podman image inspect --format "{{ .Config.Env }}" localhost/fedora-nginx
```

We can list the containers.  These will be none at least for now.  Buildah containers are a different set and do now show up with podman.  As seen above the images are shared though.

```bash
podman container ls
```

Let us now run a container based off of an image on DockerHub, "nginx:alpine".  Notice the "docker.io" in the image name indicating from the DockerHub registry.

```bash
podman run --name nginx -d -p8080:80 docker.io/library/nginx:alpine
```

We can also run our "fedora-nginx" image we created earlier.

```bash
podman run --name fedora-nginx -d -p8081:80 localhost/fedora-nginx:latest
```

Now notice there should be the two images running.

```bash
podman container ls
```

We can connect to each of them using 'curl'.  First the "docker.io/library/nginx:alpine".

```bash
curl http://localhost:8080
```

Then the "localhost/fedora-nginx:latest"

```bash
curl http://localhost:8081
```

You can also connect to these two URLs from you local browser.  Use the Public IP address you have for you lab instance.

http://{PublicHostnameOrIP}:8080

http://{PublicHostnameOrIP}:8081


See if you can now start an additional image this time using the Apache image on DockerHub, "httpd:latest".  Once it is started try connecting to it user 'curl' and via your local web browser of choice.

---

#### Cleanup

When finished exploring stop and remove all the containers and images.

```bash
podman container stop nginx
podman container stop fedora-nginx
```

```bash
podman container rm nginx
podman container rm fedora-nginx
buildah rm fedora-working-container
```

```bash
podman image rm docker.io/library/nginx:alpine
podman image rm localhost/fedora-nginx
podman image rm registry.fedoraproject.org/fedora
```

You can verify that all the containers are removed

```bash
podman container ls -a
```

If there are still any containers remaining the "prune" option is handy

```bash
podman container prune
```

Let us do the same with images

```bash
podman image ls
```

Prune also works with images

```bash
podman image prune
```

One thing to note is that typically you would not both install podman and nerdctl on the same machine.  They both come with their own version of the Container Network Interface (CNI) and depending on the versions you are running they may not be contemptible.  We are doing it here for learning purposes.  On your own choose to use either podman/skopeo/buildah or nerdctl, remember nerdctl is an all in one container solution similar to Docker but it supports more bleeding edge features.  Normally you would not need that option but make sure you include it on this machine that has both podman and nerdctl installed.

Let us remove buildah, skopeo, and podman

```bash
sudo NEEDRESTART_SUSPEND=a apt remove -y podman buildah skopeo
```

We also should clean up some no longer needed dependencies.

```bash
sudo NEEDRESTART_SUSPEND=a apt autoremove -y
```

---

This concludes part 3 of Hands-on Exercise #01.  Continue on to Part 4 next.

[Part 4: nerdctl](Part-04-nerdctl.md)
