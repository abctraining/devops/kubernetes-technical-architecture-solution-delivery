# Exercise #02 : Part 4
## K3s - Rancher Desktop

---

### Intro

We need to add a few more add-ons to our RKE2 Cluster.  Here we will add helm and OpenEBS

---

### Tab Completion

It can be helpful to enable tab-completion for 'kubectl'.  These commands will enable it.

```bash
sudo apt install -y bash-completion
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

### helm

_Add a package repo for helm_
```bash
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-add-repository "deb https://baltocdn.com/helm/stable/debian/ all main"
```

_Install the helm package_
```bash
sudo NEEDRESTART_SUSPEND=a apt install -y helm
```

_verify the install_
```bash
helm version
```

---

### OpenEBS

We will also need a storage provider for our RKE2 cluster.  We will use OpenEBS for that solution.

First we need to setup open-iscsi on each of your cluster nodes (lab#a & lab#b).  Connect to each of the cluster nodes and run the following commands.

##### iscsi on each cluster node

_Install and start open-iscsi_
```bash
sudo NEEDRESTART_SUSPEND=a apt install -y open-iscsi
sudo systemctl enable iscsid
sudo systemctl start iscsid
```

##### OpenEBS install

Now back on your primary lab instance where kubectl is configured run these commands.  We will even use the newly installed helm client.

_Install OpenEBS using Helm_
```bash
kubectl create namespace openebs
helm repo add openebs https://openebs.github.io/charts
helm repo update
helm install openebs --namespace openebs openebs/openebs
```

_wait for the resources to start_
```bash
watch kubectl get all -n openebs
```

CTRL+C will exit the "watch" command.

_Show the StorageClasses_
```bash
kubectl get sc
```

_optionally set a default StorageClass_
```bash
kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

__Note: Make sure you only set on StorageClass as "Default"__

---

This concludes part 5 of Hands-on Exercise #02.

Return to the [Exercise Page](../README.md)
