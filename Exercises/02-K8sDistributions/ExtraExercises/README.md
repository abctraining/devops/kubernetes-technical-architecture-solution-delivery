# Kubernetes - Technical Architecture Solution Delivery: K8s Distributions
# Hands-on Exercises #02

### Objective

Extra Exercise to demo Kubernetes tasks.

### Parts

[Part 1: Pods](Pods.md)

[Part 2: ReplicaSets](ReplicaSets.md)

[Part 3: Deployments](Deployments.md)

Return to the [Kubernetes Distribution Labs](../README.md)
