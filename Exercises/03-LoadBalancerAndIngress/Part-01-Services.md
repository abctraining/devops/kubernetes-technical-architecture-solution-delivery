# Exercise #03 : Part 1
## Services

---

### Intro

So far we have seen individual 'Pods' or groups of 'Pods' that are still made up of individual "Pods".  These pods represent Microservices or sometimes entire applications so they need to be connected to from clients both inside and outside the Kubernetes cluster.  The Pods come and go as the replica count or PodSpec changes.  We treat 'Pods' as ephemeral but we need a method to connect across the network to these pods that is not going to change.  Kubernetes provides the 'Service' object to do just that.  Services provide a constant IP and port for clients to connect to that is load balanced between all running replicas of the microservice / application provided by the backend pods.  You can think of the "Service" as the front door of each "microservice" within your application deployment on Kubernetes.  In this Exercise we will explore the basics exposing pods to clients through a 'Service'.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/03-LoadBalancerAndIngress
~~~

### Services

Before we get started with the LoadBalancer Exercises we first need to add a LoadBalancer provider to our RKE2 cluster.  Like most Kubernetes distributions out side of the Kubernetes as a Service versions, RKE2 does not come with a LoadBalancer provider configured.  We will use "MetalLB" as a LoadBalancer provider for our cluster.  Unfortunately we do not external IP addresses for MetalLB to use so we will be using a non-routable private IP space for the LoadBalancer IP address pool.  We will still be able to connect to them but only directly from the Kubernetes Nodes.

### Install MetalLB on RKE

We will apply two manifests; the first creates the `metallb-system` namespace, the second installs the all the MetalLB components.

_Install MetalLB_
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/metallb.yaml
```

The first time you install MetalLB you need to generate a "secretkey" that it uses internally.  __Only run this once.__  Changing the secret key later could break MetalLB communications between nodes.

_On first install Only_
```bash
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

MetalLB requires a config file to inform it about the network outside of Kubernetes.  Remember it is an "external" load-balancer and thus needs to know how to connect to external networks.  In this example we are going to give it a very basic configuration using the Layer2 protocol and a private non-routable address pool.

_Configure MetalLB_
```bash
kubectl apply -f https://gist.githubusercontent.com/thejordanclark/17dbc5c26c5383bbfbba8df9c42339c6/raw/1731799c48e356615ddce50e3d2802000440a7ab/abc-k8s-metallb-config.yaml
```

If you are curious; here is the content of that MetalLB config that was just applied.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.0.2.1-10.0.2.10
```

Now we can verify that all the MetalLB components have started up completely.

_Verify running pods_
```bash
watch kubectl get all,secrets,cm -n metallb-system
```

_Use 'CTRL+C' to exit the 'watch' command_

---

Create a pod, deployment and service using the Imperative method

Create a deployment nginx

```bash
kubectl create deployment nginx-deployment --image nginx --port 80
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

Expose the Deployment as a Service type LoadBalancer

```bash
kubectl expose deploy/nginx-deployment --type=LoadBalancer --name nginx-service
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

_Check for the nginx application from url , using the IP_

Do a describe on the nginx service , using the IP try doing a curl for that port from a shell on one of the RKE2 nodes.

```bash
kubectl describe svc nginx-service
```

verify by: `curl <ip_address>:<port_number>`

Other ways to verify from any kubectl client including from your primary Lab instance.


##### Port Forward

```bash
kubectl port-forward svc/nginx-service 8001:80 &
```

Hit enter to drob back to the shell and verify the connectivity to the service.

```bash
curl http://localhost:8001/
```

When finished bring the kubectl to the foreground and terminate the process.

```bash
fg
```

Now press __'CTRL+C'__ to terminate the kubectl port-forward process.   

---

Create a Service from a Pod (The YAML file can be found in the src directory for these exercises, listed above)

```bash
kubectl create -f sample_pod.yaml
```

Let us also create a 'jumpbox' pod that is running in the cluster.

```bash
kubectl run jumpbox --image=project42/s6-ubuntu:20.04
```

Let us install some tools in the jumpbox pod.

```bash
kubectl exec -it jumpbox -- apt update
kubectl exec -it jumpbox -- apt install -y curl dnsutils iputils-ping iproute2
```

Now with 'kubectl exec' and 'kubectl logs' we can both connect to the sample nginx pod and read the logs.


```bash
POD_IP=`kubectl describe pod/nginx-apparmor | grep -i "^IP:" | awk -F" " '{print $2}'`
kubectl exec -it jumpbox -- curl http://$POD_IP
kubectl logs pod/nginx-apparmor
```

You will see the logs

```bash
kubectl exec -it pod/nginx-apparmor -- /bin/bash
```

This will take you to the container, if pod has multiple containers, use -c option for container name.

Use `exit` to leave of the container.

Check the currently running pods

```bash
kubectl get po -o wide
```

Expose the pod as a service

```bash
kubectl expose pod nginx-apparmor --type NodePort --name nginx-apparmor-service
```

check running services again

```bash
 kubectl get po,deploy,svc,rs -o wide
```

###### verify

```bash
kubectl describe svc nginx-apparmor-service
```

Notice that the service type is now 'NodePort'.  We will see more examples in the coming sections.
### Clean up

```bash
kubectl delete svc nginx-service
kubectl delete deploy nginx-deployment
kubectl delete svc nginx-apparmor-service
kubectl delete pod nginx-apparmor
```

We will leave the 'jumpbox' pod as we can use it later.

---

This concludes part 1 of Hands-on Exercise #03.  Continue on to Part 2 next.

[Part 2: ClusterIP](Part-02-ClusterIP.md)
