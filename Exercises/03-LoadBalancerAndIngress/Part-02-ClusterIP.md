# Exercise #03 : Part 2
## ClusterIP

---

### Intro

Kubernetes Services can be one of three types. (ClusterIP, NodePort, LoadBalancer)  The low level, primary method of all connectivity to a Kubernetes Service is ultimately through a single ClusterIP.  All services regardless of type always have a ClusterIP.  As the name would indicate the ClusterIP is an IP located within the Kubernetes cluster network space.  It is a private IP address and is only accessible by itself from inside the Kubernetes Cluster.  The ClusterIP is the default type of the Kubernetes Services.  So by default services are only exposed to clients located inside the Kubernetes Cluster.

---

### ClusterIP

Let create a deployment with using 'nginx:alpine'

```bash
kubectl create deployment nginx --save-config --image=nginx:alpine --port=80 --replicas=3
```

Check to verify that the deployment is READY.

```bash
kubectl get deployment nginx
```

Also you can verify the pods, we can limit the returned pods by taking advantage of the 'selector' 'app=nginx'.  Reminder: you can always discover the selector by using  'kubectl describe' on the deployment.

```bash
kubectl get pods -l app=nginx
```

We know the pods are running.  They are listening on port 80 with nginx.  If we want to make connections to those pods we must first 'expose' the deployment with a service.  The services default to type ClusterIP.

```bash
kubectl expose deployment nginx
```

Now we can view that service, again taking advantage of the 'selector' that the 'deployment/nginx' is using.

```bash
kubectl get service -l app=nginx
```

Notice that the TYPE is 'ClusterIP' and the CLUSTER-IP is listed.  Take note of the CLUSTER-IP so we can test connectivity within the cluster.  This service can not be accessed outside of the Kubernetes cluster because it is only type 'ClusterIP'

Let us start a shell on the 'jumpbox' pod to explore from.

```bash
kubectl exec -it jumpbox -- /bin/bash
```

We can now verify that the IP address of the 'jumpbox' pod is inside the private cluster network space.   It should be an IP address that starts with a 10.x.x.x.

```bash
ip address show dev eth0
```

We also should be able to connect to the service ClusterIP directly

```bash
curl http://{Service_nginx_ClusterIP}
```

You should recieve the HTML for the nginx landing page with a <title> of "Wellcome to nginx!"

You may also notice that you can not ping the Service ClusterIP as ICMP echo requests are not allowed.

We will talk more about discovery later but for not you can see that you also can connect to that service by the service name since it is in the same Kubernetes Namespace.

```bash
curl http://nginx
```

We can also add context to the discovery

_add the namespace_
```bash
curl http://nginx.default
```

_add the object type in the namespace_
```bash
curl http://nginx.default.svc
```

_use the FQDN including the cluster ID_
```bash
curl http://nginx.default.svc.cluster.local
```

When finish you can exit the shell on the 'jumpbox' pod.  The pod will continue to run because we are only exiting the new bash shell that we started with the 'kubectl exec' command.

```bash
exit
```

---

This concludes part 2 of Hands-on Exercise #03.  Continue on to Part 3 next.

[Part 3: NodePort](Part-03-NodePort.md)
