# Exercise #03 : Part 3
## NodePort

---

### Intro

We have seen that the default type of Kubernetes services is the ClusterIP that is only accessible by internal clients to the Kubernetes cluster.  If you would like to have external clients connect to internal Services, those services need to be exposed on ports on external IP addresses.  Every Kubernetes cluster has nodes, those nodes have external IPs to the cluster.  The Service type NodePort exists to expose internal services to external clients using ports attached to those external IP address found on the cluster nodes.  The 'kube-proxy' on each node in the cluster creates the appropriate port-forwards to forward external traffic to the internal ClusterIPs of Kubernetes Services.  

---

### NodePort

We will continue where we left off from the 'ClusterIP' exercises.  Let's first verify that we have the Deployment, ReplicaSets, Pods, and Service for the 'deployment/nginx'.  We will use the selector from the deployment to verify.

```bash
kubectl get deploy,rs,po,svc -l app=nginx
```

The 'service/nginx' should still be of TYPE 'ClusterIP'.  Now let us see if we can change the Type to a 'NodePort' this time we will be using the 'kubectl patch' command to update the service spec.

```bash
kubectl patch service nginx --patch '{"spec":{"type":"NodePort"}}'
```

Use 'kubectl get' to check the service again.  Notice now the TYPE is set to 'NodePort' and the PORTS now list not only the publish port of the service (80) but also a random high port number which is the assigned NodePort for that service.  The PORTS column is formatted this way:  '{ServicePort}:{NodePort}/{TCP/UDP}'

You can Connect to the NodePort via the Public IP addresses you where given for the Kubenetes Nodes.

`http://{NodeIP}:{NodePort}`

You can use any Node IP address within your cluster as the '{NodeIP}'.  If you are curious your Node IP address are show in the INTERNAL-IP column when you display the 'wide' output of nodes.

```bash
kubectl get nodes -o wide
```

From your primary Lab instance:

```bash
curl http://{NodeIP}:{NodePort}
```

Again any cluster Node IP will work.

---

This concludes part 3 of Hands-on Exercise #03.  Continue on to Part 4 next.

[Part 4: LoadBalancer](Part-04-LoadBalancer.md)
