# Exercise #03 : Part 5
## Ingress

---

### Intro

From a networking perspective Kubernetes Services are how we get traffic to a group of pods in a Kubernetes cluster.  Every Kubernetes Service requires a dedicated external IP address.  External IP addresses are many times finite resources that each have a cost associated.  In addition to that most of the connections today utilize a web services of some sort, HTTP or one of its derivatives.  Kubernetes provides an application level load-balancer in the form a proxy with the 'Ingress' object.  In this section we will explore Kubernetes Ingress resources and IngressControllers that facilitate those Ingress resources.    

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/03-LoadBalancerAndIngress
~~~

### Ingress

#### YAML files

In that directory you will find the following manifest files for these Ingress exercises.

test-ingress.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ingress
spec:
  defaultBackend:
    service:
      name: test
      port:
        number: 80
~~~

simple-fanout-example.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: simple-fanout-example
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - path: /foo
        pathType: Prefix
        backend:
          service:
            name: service1
            port:
              number: 80
      - path: /bar
        pathType: Prefix
        backend:
          service:
            name: service2
            port:
              number: 80
~~~

name-virtual-host-ingress.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: name-virtual-host-ingress
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: service1
            port:
              number: 80
  - host: bar.foo.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: service2
            port:
              number: 80
~~~

### Ingress

Let us see what we are starting with

~~~bash
kubectl get ingress,ingressclass -A
~~~

Notice that RKE comes with the nginx IngressClass already setup.  We will utilize that IngressClass.

Create a deployment to use with Ingress

~~~bash
kubectl create deployment test --image=nginx:alpine --port=80
kubectl expose deploy/test
~~~

Apply the ingress from test-ingress.yaml

~~~bash
kubectl apply -f test-ingress.yaml
~~~

View the newly created ingress,  If you watch it for a few moments the "ADDRESS" field will populate

~~~bash
kubectl get ingress
~~~

From a cluster Node connect to site (replace the IP with the Ingress controllers external IP)

~~~bash
curl http://{Service-EXTERNAL-IP}
~~~

##### Clean-up

~~~bash
kubectl delete ingress/test-ingress
kubectl get ingress
~~~

### Fan out and Named Based split

Create additional services to connect to

~~~bash
kubectl create deployment service1 --image=httpd:alpine --port=80
kubectl create deployment service2 --image=caddy:alpine --port=80
kubectl expose deploy/service1
kubectl expose deploy/service2
~~~

Create the index

~~~bash
kubectl apply -f simple-fanout-example.yaml
kubectl get ingress
~~~

This is one of the tricker parts of the exercise as we need to get `foo.bar.com` and `bar.foo.com` to resolve to the Services ADDRESS.  There are a number of ways to do that but the simplest many times is to add a line to your system `hosts` file. We need to add a few lines to the '/etc/hosts' file on the cluster node you a testing from.  Pick one of the IP address from the ADDRESS field.  There are multiple because there are multiple nodes in the cluster to provide some redundancy.

```bash
sudo su -
echo "{Ingress-ADDRESS} foo.bar.com bar.foo.com" >> /etc/hosts
exit
```

~~~bash
curl http://foo.bar.com/foo
curl http://foo.bar.com/bar
~~~

Notice they return different services by the different paths.

##### Clean-up

~~~bash
kubectl delete -f simple-fanout-example.yaml
~~~

### Name Based

Install ingress resource

~~~bash
kubectl apply -f name-virtual-host-ingress.yaml
~~~

If everything works out correctly the new ingress should get the same IP address that the previous ingress resource had.  If not you an editor of choice and update the IP address in the '/etc/hosts' file using 'sudo' because it is owned by root.

View the resource

~~~bash
kubectl get ingress
~~~

Connect to the sites (Same issue with resolving as before, skip if needed)

~~~bash
curl http://foo.bar.com
curl http://bar.foo.com
~~~

##### Clean-up

Remove deployments

~~~bash
kubectl delete deploy/test
kubectl delete deploy/service1
kubectl delete deploy/service2
~~~

Remove sevices

~~~bash
kubectl delete svc/test
kubectl delete svc/service1
kubectl delete svc/service2
~~~

---

This concludes part 5 of Hands-on Exercise #03.

Return to the [Exercise Page](../README.md)
